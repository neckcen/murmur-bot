package main

import (
    "flag"
    "strings"
    "strconv"
    "time"

    "github.com/layeh/gumble/gumble"
    "github.com/layeh/gumble/gumbleutil"
    _ "github.com/layeh/gumble/opus"
)

func main() {
    channel := flag.String("channel", "", "Channel to move to")
    song_dir := flag.String("song", "song", "Directory where songs are located")
    song_volume := flag.Int("song-volume", 75, "Default volume (1-100)")
    effect_dir := flag.String("effect", "effect", "Directory where effects are located")
    effect_volume := flag.Int("effect-volume", 90, "Effect volume (1-100)")

    var song *SongPlayer
    var effect *EffectPlayer

    gumbleutil.Main(gumbleutil.AutoBitrate, gumbleutil.Listener{
        // Connect event
        Connect: func(event *gumble.ConnectEvent) {
            song = NewSongPlayer(event.Client, *song_dir, *song_volume)
            effect = NewEffectPlayer(event.Client, *effect_dir, *effect_volume)
            // move to given channel if possible
            if *channel != "" {
                c := event.Client.Channels.Find(*channel)
                if c != nil {
                    event.Client.Self.Move(c)
                }
            }
        },

        // Text message event
        TextMessage: func(event *gumble.TextMessageEvent) {
            // play an effect
            if strings.HasPrefix(event.Message, "#") {
                if effect.Playing() {
                    return
                }
                _, ok := effect.files[event.Message[1:]]
                if ! ok {
                    return
                }
                go func() {
                    interupt := song.Pause()
                    if interupt {
                        time.Sleep(100 * time.Millisecond)
                    }
                    effect.Play(event.Message[1:])
                    if interupt {
                        effect.stream.Wait()
                        time.Sleep(100 * time.Millisecond)
                        song.Play()
                    }
                }()
            }
            // ignore messages not starting with !
            if !strings.HasPrefix(event.Message, "!") {
                return
            }
            switch {
                case event.Message == "!help":
                    event.Sender.Send(
                        "<strong>Available commands:</strong><br/>"+
                        "  <strong>!help</strong> Display this help.<br/>"+
                        "  <strong>!play</strong> Play/resume music.<br/>"+
                        "  <strong>!pause</strong> Pause music.<br/>"+
                        "  <strong>!stop</strong> Stop music.<br/>"+
                        "  <strong>!stopafter</strong> Stop music after the current song.<br/>"+
                        "  <strong>!next</strong> Skip to next song.<br/>"+
                        "  <strong>!prev</strong> Skip to previous song.<br/>"+
                        "  <strong>!loop</strong> Loop current song.<br/>"+
                        "  <strong>!replay</strong> Restart current song.<br/>"+
                        "  <strong>!song</strong> Display current song.<br/>"+
                        "  <strong>!vol 1-100</strong> Set volume.<br/>"+
                        "  <strong>!quiet</strong> Set volume to 7.<br/>"+
                        "  <strong>!loud</strong> Set volume to 75.<br/>"+
                        "  <strong>!effect</strong> List effects.")

                case event.Message == "!play":
                    song.Play()

                case event.Message == "!pause":
                    song.Pause()

                case event.Message == "!stop":
                    song.Stop()

                case event.Message == "!stopafter":
                    song.StopAfter()

                case event.Message == "!next":
                    song.Next()

                case event.Message == "!prev":
                    song.Prev()

                case event.Message == "!loop":
                    song.Loop()

                case event.Message == "!replay":
                    song.Replay()

                case event.Message == "!song":
                    event.Sender.Send(song.Current())

                case event.Message == "!loud":
                    song.Volume(75)

                case event.Message == "!quiet":
                    song.Volume(7)

                case strings.HasPrefix(event.Message, "!vol "):
                    i, _ := strconv.Atoi(event.Message[5:])
                    song.Volume(i)

                case event.Message == "!effect":
                    effect_list := "<strong>Available effects:</strong><br/>"
                    for e := range effect.files {
                        effect_list += "#"+e+" "
                    }
                    event.Sender.Send(effect_list)
            }
        },
    })
}

