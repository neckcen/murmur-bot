package main

import (
    "fmt"
    "os"
    "path/filepath"
    "math/rand"
    "time"

    "github.com/layeh/gumble/gumble"
    "github.com/layeh/gumble/gumbleffmpeg"
)

/*******************************************
 *
 *  Player
 *
 ******************************************/
type Player struct {
    client *gumble.Client
    stream *gumbleffmpeg.Stream
    volume float32
}

// set the player's volume
func (player *Player) Volume (volume int) {
    if volume > 100 {
        volume = 100
    } else if volume < 1 {
        volume = 1
    }
    player.volume = float32(volume)/100.
    player.stream.Volume = player.volume
}

// whether the player is playing
func (player *Player) Playing () bool {
    return player.stream.State() == gumbleffmpeg.StatePlaying
}

// whether the player is paused
func (player *Player) Paused () bool {
    return player.stream.State() == gumbleffmpeg.StatePaused
}

// play a file
func (player *Player) Play (file string) {
    if player.Paused() {
        player.stream.Play()
        return
    }
    if player.Playing() {
        return
    }
    player.stream = gumbleffmpeg.New(player.client, gumbleffmpeg.SourceFile(file))
    player.stream.Volume = player.volume
    err := player.stream.Play()
    if err != nil {
        fmt.Printf("%s\n", err)
    }
}

// pause the player
func (player *Player) Pause () bool {
    if player.stream.Pause() != nil {
        return false
    }
    return true
}

// stop the player
func (player *Player) Stop () bool {
    if player.stream.Stop() != nil {
        return false
    }
    return true
}

func NewPlayer (client *gumble.Client, volume int) *Player {
    player := &Player{
        client: client,
        stream: gumbleffmpeg.New(nil, nil), // saves us checking for nil
    }
    player.Volume(volume)
    return player
}

/*******************************************
 *
 *  SongPlayer
 *
 ******************************************/
type SongPlayer struct {
    *Player

    // what to do when the current song ends
    callback func()

    files []string

    // shuffle data
    shuffle []int
    shuffle_index int
}

// play a random song
func (player *SongPlayer) Play () {
    player.callback = player.c_next
    if player.Paused() {
        player.stream.Play()
        return
    }
    if player.Playing() {
        return
    }
    // Return random music file
    if player.shuffle_index >= len(player.shuffle) || player.shuffle_index < 0 {
        rand.Seed(time.Now().Unix())
        player.shuffle = rand.Perm(len(player.files))
        player.shuffle_index = 0
    }

    player.Player.Play(player.files[player.shuffle[player.shuffle_index]])

    go func() {
        player.stream.Wait()
        if player.callback != nil {
            player.callback()
        }
    }()
}

// callbacks
func (player *SongPlayer) c_prev () {
    player.shuffle_index--
    player.Play()
}
func (player *SongPlayer) c_loop () {
    player.Play()
    player.callback = player.c_loop
}
func (player *SongPlayer) c_next () {
    player.shuffle_index++
    player.Play()
}

// go back to previous song
func (player *SongPlayer) Prev () {
    player.callback = player.c_prev
    player.stream.Stop()
}

// restart current song
func (player *SongPlayer) Replay () {
    player.Stop()
    player.Play()
}

// loop current song
func (player *SongPlayer) Loop () {
    player.callback = player.c_loop
}

// skip to next song
func (player *SongPlayer) Next () {
    player.callback = player.c_next
    player.stream.Stop()
}

// stop playing
func (player *SongPlayer) Stop () {
    player.callback = nil
    player.Player.Stop()
}

// stop playing after the current song
func (player *SongPlayer) StopAfter () {
    player.callback = nil
}

// current song
func (player *SongPlayer) Current () string {
    if player.Playing() || player.Paused() {
        file := player.files[player.shuffle[player.shuffle_index]]
        ext := filepath.Ext(file)
        file = filepath.Base(file)
        file = file[0:len(file)-len(ext)]
        return "Now playing: "+file
    }
    return "Not currently playing."
}

func NewSongPlayer (client *gumble.Client, dir string, volume int) *SongPlayer {
    files, err := filepath.Glob(dir+"/*")
    if err != nil {
        fmt.Printf("%s\n", err)
        os.Exit(1)
    }
    fmt.Printf("Loaded %d songs.\n", len(files))

    return &SongPlayer{
        Player: NewPlayer(client, volume),
        files: files,
    }
}

/*******************************************
 *
 *  EffectPlayer
 *
 ******************************************/
type EffectPlayer struct {
    *Player

    files map [string]string
}

// play an effect
func (player *EffectPlayer) Play (effect string) {
    // check the effect exists
    file, ok := player.files[effect]
    if !ok {
        return
    }

    player.Player.Play(file)
}

func NewEffectPlayer(client *gumble.Client, dir string, volume int) *EffectPlayer {
    // load effect files
    files_, err := filepath.Glob(dir+"/*")
    if err != nil {
        fmt.Printf("%s\n", err)
        os.Exit(1)
    }
    files := make(map [string]string)
    for _, file := range files_ {
        key := filepath.Base(file)
        key = key[:len(key)-len(filepath.Ext(file))]
        files[key] = file
    }
    fmt.Printf("Loaded %d effect files.\n", len(files))

    return &EffectPlayer{
        Player: NewPlayer(client, volume),
        files: files,
    }
}
